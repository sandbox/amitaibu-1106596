<?php

/**
 * Implementation of hook_default_og_membership_type().
 */
function og_membership_expire_default_og_membership_type() {
  $items = array();
  $items['og_membership_expire'] = entity_import('og_membership_type', '{
    "name" : "og_membership_expire",
    "description" : "Expire",
    "rdf_mapping" : []
  }');
  return $items;
}
