<?php

/**
 * Implementation of hook_default_rules_configuration().
 */
function og_membership_expire_default_rules_configuration() {
  $items = array();
  $items['rules_og_expire'] = entity_import('rules_config', '{ "rules_og_expire" : {
      "LABEL" : "OG expire",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "og" ],
      "ON" : [ "og_membership_update" ],
      "IF" : [
        { "data_is" : { "data" : [ "og-membership:name" ], "value" : "og_membership_expire" } },
        { "data_is" : { "data" : [ "og-membership:state" ], "value" : "2" } },
        { "data_is" : { "data" : [ "og-membership:entity-type" ], "value" : "user" } }
      ],
      "DO" : [
        { "entity_fetch" : {
            "USING" : { "type" : "user", "id" : [ "og-membership:etid" ] },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "User" } }
          }
        },
        { "entity_fetch" : {
            "USING" : { "type" : "group", "id" : [ "og-membership:gid" ] },
            "PROVIDE" : { "entity_fetched" : { "og_group_fetched" : "OG group" } }
          }
        },
        { "mail" : {
            "to" : [ "entity-fetched:mail" ],
            "subject" : "Your membership has expired [og_group_fetched:label]",
            "message" : "Dear [entity_fetched:name],\\r\\nYour membership has expired in group [og_group_fetched:label] -- [og_group_fetched:url]\\r\\n\\r\\n[site:name]\\t",
            "from" : ""
          }
        }
      ]
    }
  }');
  return $items;
}
