<?php

/**
 * Implementation of hook_field_default_fields().
 */
function og_membership_expire_field_default_fields() {
  $fields = array();

  // Exported field: 'og_membership-og_membership_expire-field_og_expire_datestamp'
  $fields['og_membership-og_membership_expire-field_og_expire_datestamp'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_og_expire_datestamp',
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'date',
      'settings' => array(
        'granularity' => array(
          'day' => 'day',
          'hour' => 'hour',
          'minute' => 'minute',
          'month' => 'month',
          'year' => 'year',
        ),
        'profile2_private' => FALSE,
        'repeat' => 0,
        'timezone_db' => 'UTC',
        'todate' => '',
        'tz_handling' => 'site',
      ),
      'translatable' => '1',
      'type' => 'datestamp',
    ),
    'field_instance' => array(
      'bundle' => 'og_membership_expire',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'date',
          'settings' => array(
            'format_type' => 'long',
            'fromto' => 'both',
            'multiple_from' => '',
            'multiple_number' => '',
            'multiple_to' => '',
            'show_repeat_rule' => 'show',
          ),
          'type' => 'date_default',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'og_membership',
      'field_name' => 'field_og_expire_datestamp',
      'label' => 'Expire',
      'required' => 1,
      'settings' => array(
        'default_format' => 'medium',
        'default_value' => 'strtotime',
        'default_value2' => 'blank',
        'default_value_code' => '+1 month',
        'default_value_code2' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'date',
        'settings' => array(
          'increment' => 1,
          'input_format' => 'm/d/Y - H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'repeat_collapsed' => 0,
          'text_parts' => array(),
          'year_range' => '-3:+3',
        ),
        'type' => 'date_text',
        'weight' => '1',
      ),
    ),
  );

  return $fields;
}
